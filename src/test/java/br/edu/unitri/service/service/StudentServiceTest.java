package br.edu.unitri.service.service;

import br.edu.unitri.service.domain.Student;
import br.edu.unitri.service.repository.StudentRepository;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {


    @Mock
    private StudentRepository repository;

    @Mock
    private MeterRegistry registry;

    @Test
    public void testFindStudentHappyPath() {


        StudentService unit = new StudentService(repository, registry);

        when(repository.findById("john")).thenReturn(Optional.of(new Student("1", "john", "st", 1L)));

        Optional<Student> result = unit.getStudentByCode("john");

        assertTrue(result.isPresent());
        assertEquals(result.get().getName(), "john");
        assertEquals(result.get().getStudentCode(), "1");
    }

    @Test
    public void testFindStudentNotFound() {


        StudentService unit = new StudentService(repository, registry);

        when(repository.findById("john")).thenReturn(Optional.empty());

        Optional<Student> result = unit.getStudentByCode("john");

        assertFalse(result.isPresent());
    }
}
