
CREATE SEQUENCE STUDENT_CODE_SEQ;

CREATE TABLE tb_student (
    studentcode        NUMBER(20) NOT NULL,
    name               VARCHAR2(256) NOT NULL,
    address            VARCHAR2(500) NOT NULL,
    citycode           VARCHAR2(100) NOT NULL,
    memo               VARCHAR2(3000)
);

CREATE TABLE TS_RESULT_TESTE_DESEMPENHO
(COD_SESSAO  NUMBER(3)    NOT NULL
,ETAPA       VARCHAR2(50) NOT NULL
,DT_HORA_INI DATE
,DT_HORA_FIM DATE
);

CREATE TABLE TS_TESTE_DESEMPENHO
(COD_SESSAO    NUMBER(3)  NOT NULL
,COD_SEQUENCIA NUMBER(10) NOT NULL
,DESCRICAO     VARCHAR2(500)
);

CREATE TABLE TS_TRAVA_TESTE_DESEMPENHO
(COD_SESSAO NUMBER(3) NOT NULL
);

--CREATE OR REPLACE PROCEDURE PS_TESTE_DESEMPENHO
--(pCod_Sessao     NUMBER
--,pQtde_Registros NUMBER
--,pQtde_Commit    NUMBER
--,pNro_Etapas     NUMBER)
--IS
-- PROCEDURE INICIA_ETAPA(pEtapa VARCHAR2) IS
-- BEGIN
--   INSERT INTO TS_RESULT_TESTE_DESEMPENHO
--     (COD_SESSAO,ETAPA,DT_HORA_INI)
--     VALUES (pCod_Sessao,pEtapa,SYSDATE);
--   COMMIT;
-- END;
-- PROCEDURE FINALIZA_ETAPA(pEtapa VARCHAR2) IS
-- BEGIN
--   UPDATE TS_RESULT_TESTE_DESEMPENHO
--   SET DT_HORA_FIM = SYSDATE
--   WHERE COD_SESSAO = pCod_Sessao
--     AND ETAPA = pEtapa;
--   COMMIT;
-- END;
-- PROCEDURE EXECUTA_ETAPA_1 IS
--   vCod_Sequencia TS_TESTE_DESEMPENHO.COD_SEQUENCIA%TYPE;
--   vQtde_Commit_Aux NUMBER;
-- BEGIN
--   INICIA_ETAPA('TESTE 1');
--   vCod_Sequencia := 1;
--   vQtde_Commit_Aux := 0;
--   LOOP
--     INSERT INTO TS_TESTE_DESEMPENHO
--       (COD_SESSAO,COD_SEQUENCIA,DESCRICAO)
--        VALUES (pCod_Sessao,vCod_Sequencia,'Descrição de teste da etapa 1 : Sequência = ' || TO_CHAR(vCod_Sequencia));
--     vQtde_Commit_Aux := vQtde_Commit_Aux + 1;
--     IF vQtde_Commit_Aux >= pQtde_Commit
--     THEN COMMIT;
--          vQtde_Commit_Aux := 0;
--     END IF;
--     EXIT WHEN vCod_Sequencia >= pQtde_Registros;
--     vCod_Sequencia := vCod_Sequencia + 1;
--   END LOOP;
--   COMMIT;
--   FINALIZA_ETAPA('TESTE 1');
-- END;
-- PROCEDURE EXECUTA_ETAPA_2 IS
--   vCod_Sequencia TS_TESTE_DESEMPENHO.COD_SEQUENCIA%TYPE;
--   vQtde_Commit_Aux NUMBER;
--   vCod_Busca TS_TESTE_DESEMPENHO.COD_SEQUENCIA%TYPE;
-- BEGIN
--   INICIA_ETAPA('TESTE 2');
--   vCod_Sequencia := 1;
--   vQtde_Commit_Aux := 0;
--   LOOP
--     vCod_Busca := DBMS_RANDOM.VALUE(1,pQtde_Registros);
--     UPDATE TS_TESTE_DESEMPENHO
--     SET DESCRICAO = 'Descrição de teste etapa 2 : Sequencia = ' || TO_CHAR(vCod_Sequencia)
--     WHERE COD_SESSAO = pCod_Sessao
--       AND COD_SEQUENCIA = vCod_Busca;
--     vQtde_Commit_Aux := vQtde_Commit_Aux + 1;
--     IF vQtde_Commit_Aux >= pQtde_Commit
--     THEN COMMIT;
--          vQtde_Commit_Aux := 0;
--     END IF;
--     EXIT WHEN vCod_Sequencia >= pQtde_Registros;
--     vCod_Sequencia := vCod_Sequencia + 1;
--   END LOOP;
--   COMMIT;
--   FINALIZA_ETAPA('TESTE 2');
-- END;
-- PROCEDURE EXECUTA_ETAPA_3 IS
--   vCod_Sequencia TS_TESTE_DESEMPENHO.COD_SEQUENCIA%TYPE;
--   vQtde_Commit_Aux NUMBER;
--   vCod_Busca TS_TESTE_DESEMPENHO.COD_SEQUENCIA%TYPE;
-- BEGIN
--   INICIA_ETAPA('TESTE 3');
--   vCod_Sequencia := 1;
--   vQtde_Commit_Aux := 0;
--   LOOP
--     vCod_Busca := DBMS_RANDOM.VALUE(1,pQtde_Registros);
--     DELETE FROM TS_TESTE_DESEMPENHO
--     WHERE COD_SESSAO = pCod_Sessao
--       AND COD_SEQUENCIA = vCod_Busca;
--     vQtde_Commit_Aux := vQtde_Commit_Aux + 1;
--     IF vQtde_Commit_Aux >= pQtde_Commit
--     THEN COMMIT;
--          vQtde_Commit_Aux := 0;
--     END IF;
--     EXIT WHEN vCod_Sequencia >= pQtde_Registros;
--     vCod_Sequencia := vCod_Sequencia + 1;
--   END LOOP;
--   COMMIT;
--   FINALIZA_ETAPA('TESTE 3');
-- END;
--BEGIN
-- DELETE FROM TS_RESULT_TESTE_DESEMPENHO
-- WHERE COD_SESSAO = pCod_Sessao;
-- INSERT INTO TS_TRAVA_TESTE_DESEMPENHO VALUES (pCod_Sessao);
-- IF pNro_Etapas >= 1
-- THEN EXECUTA_ETAPA_1;
-- END IF;
-- IF pNro_Etapas >= 2
-- THEN EXECUTA_ETAPA_2;
-- END IF;
-- IF pNro_Etapas >= 3
-- THEN EXECUTA_ETAPA_3;
-- END IF;
--END;