package br.edu.unitri.service.controller;

import br.edu.unitri.service.domain.Student;
import br.edu.unitri.service.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("students")
class StudentController {


    private StudentService service;

    @Autowired
    public StudentController(StudentService service) {

        this.service = service;
    }

    @GetMapping("/count")
    public Long countStudents() {

        return service.countStudents();
    }

    @PostMapping
    public void saveStudent(@RequestBody Student student) {

        service.saveStudent(student);
    }

    @GetMapping
    public List<Student> getStudents() {

        return service.getStudent();
    }
}