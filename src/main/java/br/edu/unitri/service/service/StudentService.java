package br.edu.unitri.service.service;

import br.edu.unitri.service.Application;
import br.edu.unitri.service.domain.Student;
import br.edu.unitri.service.repository.StudentRepository;
import com.google.common.collect.Lists;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private StudentRepository repository;

    private MeterRegistry metricsRegistry;

    @Autowired
    public StudentService(StudentRepository repository, MeterRegistry metricsRegistry) {

        this.repository = repository;
        this.metricsRegistry = metricsRegistry;
    }

    public Long countStudents() {

        return repository.count();
    }

    public List<Student> getStudent() {

        return Lists.newArrayList(repository.findAll());
    }

    @HystrixCommand(fallbackMethod = "saveStudentFallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
            @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10")

    })
    public void saveStudent(Student student) {

        metricsRegistry.counter("student.save").increment();
        repository.save(student);
    }

    public void saveStudentFallback(Student student, Throwable e) {

        log.info("erro ao gravar estudante {}", student);
        metricsRegistry.counter("student.save.fallback").increment();
    }

    public Optional<Student> getStudentByCode(String id) {

        return repository.findById(id);
    }

    public void deleteStudent(Student student) {

        repository.delete(student);
    }
}