package br.edu.unitri.service.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


@Entity
@Table(name = "TB_STUDENT")
public class Student {

    public Student() {

    }
    public Student(String studentCode, String name, String address, Long citycode) {
        this.studentCode = studentCode;
        this.name = name;
        this.address = address;
        this.citycode = citycode;
    }

    @Id
    @Column(name = "studentcode")
    private String studentCode;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "citycode")
    private Long citycode;

    @Column(name = "memo")
    private Long memo;

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCitycode() {
        return citycode;
    }

    public void setCitycode(Long citycode) {
        this.citycode = citycode;
    }

    public Long getMemo() {
        return memo;
    }

    public void setMemo(Long memo) {
        this.memo = memo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(studentCode, student.studentCode) &&
                Objects.equals(name, student.name) &&
                Objects.equals(address, student.address) &&
                Objects.equals(citycode, student.citycode) &&
                Objects.equals(memo, student.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentCode, name, address, citycode, memo);
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentCode='" + studentCode + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", citycode=" + citycode +
                ", memo=" + memo +
                '}';
    }
}
