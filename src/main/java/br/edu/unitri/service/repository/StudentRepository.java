package br.edu.unitri.service.repository;

import br.edu.unitri.service.domain.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, String> {
}
